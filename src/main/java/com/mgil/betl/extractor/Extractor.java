package com.mgil.betl.extractor;

import com.mgil.betl.savepoint.SavePoint;

import java.util.Collection;

public abstract class Extractor<T> {

    protected int pageSize;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public abstract void initialize();
    public abstract Collection<T> read();
    public abstract boolean hasNext();
    public abstract void loadPreviousState(SavePoint savePoint);

}
