package com.mgil.betl.listener;

public interface ProgressListener<T> {
    void notifyProgress(T progress);
}
