package com.mgil.betl.listener;

public interface ETLListener {
    void onStart();
    void onStop();
    void onFail(Throwable T);
}
