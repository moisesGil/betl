package com.mgil.betl.coordinator;

import com.mgil.betl.empty.EmptyEtlListener;
import com.mgil.betl.empty.EmptyProgressListener;
import com.mgil.betl.empty.EmptySavePointManager;
import com.mgil.betl.extractor.Extractor;
import com.mgil.betl.listener.ETLListener;
import com.mgil.betl.listener.ProgressListener;
import com.mgil.betl.loader.Loader;
import com.mgil.betl.savepoint.SavePoint;
import com.mgil.betl.savepoint.SavePointManager;
import com.mgil.betl.transformer.Transformer;

import java.util.Collection;

public class ETLCoordinator {

    private Extractor extractor;
    private Transformer transformer;
    private Loader loader;
    private SavePointManager savePointManager = new EmptySavePointManager();
    private ETLListener etlListener = new EmptyEtlListener();
    private ProgressListener progressListener = new EmptyProgressListener();
    private int pageSize;
    private static ETLCoordinator instance;

    private ETLCoordinator() {

    }

    public static ETLCoordinator create() {

        if (instance == null) {
            instance = new ETLCoordinator();
        }

        return instance;

    }

    public ETLCoordinator withExtractor(Extractor extractor) {
        this.extractor = extractor;
        return this;
    }

    public ETLCoordinator withTransformer(Transformer transformer) {
        this.transformer = transformer;
        return this;
    }

    public ETLCoordinator withLoader(Loader loader) {
        this.loader = loader;
        return this;
    }

    public ETLCoordinator withPaginationSize(int size) {
        pageSize = size;
        extractor.setPageSize(pageSize);
        return this;
    }


    public ETLCoordinator withSavePointManager(SavePointManager savePointManager) {
        this.savePointManager = savePointManager;
        return this;
    }

    public ETLCoordinator withProgressListener(ProgressListener progressListener) {
        this.progressListener = progressListener;
        return this;
    }


    public ETLCoordinator withListener(ETLListener listener) {
        this.etlListener = listener;
        return this;
    }

    public void start() {

        try {

            etlListener.onStart();
            {
                extractor.initialize();

                if (savePointManager.existAnySavePoint())
                    extractor.loadPreviousState(savePointManager.loadSavePoint());

                while (extractor.hasNext()) {

                    Collection elements = extractor.read();
                    Collection converted = transformer.transform(elements);
                    progressListener.notifyProgress(loader.load(converted));

                    if (savePointManager != null) {
                        SavePoint currentState = loader.createSavePoint();
                        savePointManager.persistSavePoint(currentState);
                    }

                }

                savePointManager.cleanSavepointOnSuccess();
            }
            etlListener.onStop();

        } catch (Exception ex) {
            etlListener.onFail(ex);
            ex.printStackTrace();
        }

    }

}
