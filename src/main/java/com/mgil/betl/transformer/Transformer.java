package com.mgil.betl.transformer;

import java.util.Collection;

public interface Transformer<O,T> {
    Collection<T> transform(Collection<O> toTransform);
}
