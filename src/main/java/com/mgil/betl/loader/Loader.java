package com.mgil.betl.loader;

import com.mgil.betl.savepoint.SavePoint;

import java.util.Collection;

public interface Loader<T,P> {
    P load(Collection<T> dataToLoad);
    SavePoint createSavePoint();
}
