package com.mgil.betl.savepoint;


import java.util.Date;

public class SavePoint<T> {

    protected Date startDate;
    protected Date endDate;
    protected T lastWorkState;

    public SavePoint() {
    }

    public SavePoint(Date startDate, Date endDate, T lastWorkState) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastWorkState = lastWorkState;

    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public T getLastWorkState() {
        return lastWorkState;
    }

    public void setLastWorkState(T lastWorkState) {
        this.lastWorkState = lastWorkState;
    }
}
