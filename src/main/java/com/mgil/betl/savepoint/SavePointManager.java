package com.mgil.betl.savepoint;

public abstract class SavePointManager {

    protected SavePointSource source;

    public abstract boolean existAnySavePoint();
    public abstract SavePoint loadSavePoint();
    public abstract void persistSavePoint(SavePoint savePoint);
    public abstract void cleanSavepointOnSuccess();


}
