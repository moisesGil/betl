package com.mgil.betl.savepoint;

public abstract class SavePointSource<C> {
    public abstract C getSource();
}
