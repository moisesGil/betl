package com.mgil.betl.empty;

import com.mgil.betl.savepoint.SavePoint;
import com.mgil.betl.savepoint.SavePointManager;

public class EmptySavePointManager extends SavePointManager {

    public boolean existAnySavePoint() {
        return false;
    }

    public SavePoint loadSavePoint() {
        return null;
    }

    public void persistSavePoint(SavePoint savePoint) {

    }

    public void cleanSavepointOnSuccess() {

    }
}
